#ifndef TWI_MASTER_H
#define TWI_MASTER_H

#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#include <avr/io.h>
#include <util/twi.h>
#include <stdint.h>

#define SCL_FREQ 90000 
#define TWI_PRES 1

#if TWI_PRES == 1
    #define TWI_PRES_TWSR 0 
#elif TWI_PRES == 4
    #define TWI_PRES_TWSR 1
#elif TWI_PRES == 16
    #define TWI_PRES_TWSR 2
#elif TWI_PRES == 64
    #define TWI_PRES_TWSR 3
#else 
    #define TWI_PRES_TWSR 0
    #warning "TWI bit rate prescaler set incorrectly (must be either 1, 4, 16 or 64)! Defaulting to 1"
#endif

#define TWI_RATE (F_CPU-16*SCL_FREQ)/(2*SCL_FREQ*TWI_PRES) //calculates the TWBR value  

/*Some definitions that are handy while using this library*/

#define TWI_ACK 0
#define TWI_NACK 1
#define TWI_READ 1
#define TWI_WRITE 0

/*
 * @brief Initializes the TWI for master operation
 */
extern void twi_master_init(void);

/*
 * @brief Generates a start condition and addresses a slave 
 * @param addr: the slave to be addressed
 *        rw: the value of the R/W bit, either READ or WRITE
 * @return the value of the ACK bit, either NACK or ACK 
 */
extern uint8_t twi_start(uint8_t addr, uint8_t rw);

/*
 * @brief Writes one byte to an already addressed slave
 * @param data: the byte to transmit
 * @return the value of the ACK bit, either ACK or NACK 
 */
extern uint8_t twi_write(uint8_t data);

/*
 * @brief Reads one byte from an already addressed slave
 * @param The value of the ACK bit the master transmits, either ACK or NACK
 * @return The byte receive from the slave 
 */
extern uint8_t twi_read(uint8_t ack);

/*
 * @brief Generates a stop condition
 */
extern void twi_stop(void);

#endif /* TWI_MASTER_H */

