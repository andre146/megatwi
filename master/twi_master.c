#ifndef TWI_MASTER_C
#define TWI_MASTER_C

#include "twi_master.h"

void twi_master_init(){
    TWSR = TWI_PRES_TWSR; //set the prescaler
    TWBR = TWI_RATE; //set the bit rate register
    TWCR = (1<<TWEN); //enable the TWI 
}

uint8_t twi_start(uint8_t addr, uint8_t rw){ 
    TWCR |= (1<<TWSTA) | (1<<TWINT); //generate start condition
    while(!(TWCR & (1<<TWINT))); //wait for start condition to be executed
    TWCR &= ~(1<<TWINT) & ~(1<<TWSTA); //clear start condition bit
    TWDR = (addr << 1) | rw; //send SLA+RW
    TWCR |= (1<<TWINT); 
    while(!(TWCR & (1<<TWINT)));
    return(!(TW_STATUS == TW_MT_SLA_ACK || TW_STATUS == TW_MR_SLA_ACK));
}

uint8_t twi_write(uint8_t data){
    TWDR = data; //transmit the data byte
    TWCR |= (1<<TWINT);
    while(!(TWCR & (1<<TWINT)));
    if(TW_STATUS == TW_BUS_ERROR || TW_STATUS == TW_MT_ARB_LOST){ //reset the interface if an error occurs
        twi_stop();
        TWCR &= ~(1<<TWEN);
        TWCR = (1<<TWEN) | (1<<TWINT);
    }
    return(!(TW_STATUS == TW_MT_DATA_ACK));
}

uint8_t twi_read(uint8_t ack){
    TWCR |= (1<<TWINT) | ((!ack)<<TWEA); //wait for data byte to come in and send ACK bit
    while(!(TWCR & (1<<TWINT)));
    TWCR &= ~(1<<TWINT) & ~(1<<TWEA); //reset ACK bit for the next time
    if(TW_STATUS == TW_BUS_ERROR || TW_STATUS == TW_MR_ARB_LOST){ //reset the interface if an error occurs
        twi_stop();
        TWCR &= ~(1<<TWEN);
        TWCR = (1<<TWEN) | (1<<TWINT);
    }
    return(TWDR);
}

void twi_stop(){
    TWCR |= (1<<TWSTO) | (1<<TWINT); //generate a stop condition
}

#endif
