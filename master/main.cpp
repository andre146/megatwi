#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#define __AVR_ATmega328__
#define SLAVE_ADDR 10

#include <avr/io.h>
#include "twi_master.h"
#include "twi_master.c"

int main(void) {
    
    twi_master_init(); //initialize the interface
    unsigned char buffer[2];
    
    /*start a transmission to slave 10 in Master transmitter mode and check if the slave responded with ACK*/
    if(twi_start(SLAVE_ADDR, WRITE) == ACK){ 
        twi_write(1); //write data to the slave
        twi_write(255); 
        if(twi_start(SLAVE_ADDR, READ) == ACK){ //send a repeated start condion in master receiver mode
            buffer[0] = twi_read(ACK); //read the first byte from the slave
            buffer[1] = twi_read(NACK); //read the last byte from the slave (hence NACK instead of ACK)
        }
    } 
    twi_stop(); //stop the transmission and release the bus
    
    while (1) {
        
    }
    
}
