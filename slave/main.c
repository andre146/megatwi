#ifndef F_CPU
#define F_CPU 16000000
#endif

#define __AVR_ATmega328__

#include <stdio.h>
#include <avr/io.h>
#include <string.h>
#include <util/delay.h>
#include "slave.h"
#include "../../Geigerzähler/uart/uart.h"
#include "../../Geigerzähler/uart/uart.c"

void delayMs(uint16_t ms){
    for(; ms > 0; ms--){
         _delay_ms(1);
    }
}

int main(int argc, char** argv) {

    uart_init(UART_BAUD_SELECT(19200, F_CPU));
    
    uint8_t data[TWI_BUFFER_SIZE];
    
    for(uint8_t i = 0; i < TWI_BUFFER_SIZE; i++){
        data[i] = i*2;
        twi_data_buffer[i] = &data[i];
    }
    
    twi_slave_init(10, 0);
    char str_buffer[8];
    
    sei();
    
    while(1){
        for(uint8_t i = 0; i<TWI_BUFFER_SIZE; i++){
            sprintf(str_buffer, "%i\n", data[i]);
            uart_puts(str_buffer);
        }
        delayMs(500);
    }        
    
    return (0);
}

