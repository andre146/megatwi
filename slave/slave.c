#include "slave.h"

ISR(TWI_vect){
    switch(TWSR) {
        case TW_SR_GCALL_ACK:
        case TW_SR_SLA_ACK:
            twi_ptr_set = 0;
            break;
        
        case TW_SR_GCALL_DATA_ACK:
        case TW_SR_DATA_ACK:
            if(!twi_ptr_set){
                twi_data_buffer_index = TWDR;
                twi_ptr_set = 1;
            }else{
                *twi_data_buffer[twi_data_buffer_index] = TWDR;
                if(twi_data_buffer_index == TWI_BUFFER_SIZE - 1){
                    twi_data_buffer_index = 0;
                }else{
                    twi_data_buffer_index++;
                }
            }
            break;
            
        case TW_ST_DATA_ACK:
        case TW_ST_SLA_ACK:
            TWDR = *twi_data_buffer[twi_data_buffer_index];
            if(twi_data_buffer_index == TWI_BUFFER_SIZE - 1){
                twi_data_buffer_index = 0;
            }else{
                twi_data_buffer_index++;
            }
            break;
            
        case TW_BUS_ERROR: 
            TWCR |= (1<<TWSTO) | (1<<TWINT);
    }

    TWCR = (1<<TWINT);
}

void twi_slave_init(uint8_t addr, uint8_t general_call){
    TWAR = (addr << 1) | general_call ? 1 : 0;
    TWCR = (1<<TWEN) | (1<<TWEA) | (1<<TWIE);
}