#ifndef TWI_SLAVE_C
#define TWI_SLAVE_C

#include <avr/io.h>
#include <stdlib.h>
#include <util/twi.h>
#include <avr/interrupt.h>

#define TWI_BUFFER_SIZE 8

uint8_t twi_ptr_set = 0;
uint8_t twi_data_recv = 0;
uint8_t *twi_data_buffer[TWI_BUFFER_SIZE];
uint8_t twi_data_buffer_index = 0;

extern void twi_slave_init(uint8_t addr, uint8_t general_call);

#endif